const express = require('express'),
productRouter = express.Router(),
Product = require('../Models/product.model');
Company = require('../Models/company.model');

//Rutas insertar, consultar, modificar, eliminar articulo    
    productRouter.use('/product/:id', (req, res, next)=>{
        const id = req.params.id;
        Product.findById(id, (err, product)=>{
            if(err){
                res.status(500);}
            else if(product){
                req.product = product;
                next();
            } else
                res.status(404).send('No product was found');
        })
    });

    productRouter.route('/product/:id')
    .get((req, res) => {
        console.log('error');
        res.json(req.product);
    })
    .delete((req, res) => {
        req.product.remove((err)=>{
            if(err){
                res.status(500).send(err);
            } else {
                res.send('Removed');
            }
        });
    });
    
    productRouter.use('/product', async (req, res, next)=>{
        const categories = req.body.categories;
        const productLines = req.body.productLines;
        const companyId = req.body.company;
        const company = await Company.findById(companyId);
        if (!company)
            res.status(500).send('no company was found');
        else if (!handleCategoryProductLine(company.categories, categories))
            res.status(500).send('at least one category not exist');
        else if(!handleCategoryProductLine(company.productLine, productLines))
            res.status(500).send('at least one productLines not exist');
        else
            next();
            
        function handleCategoryProductLine(bdField, bodyField) {
            let success = false;
            for(let j=0;j < bodyField.length; j++){
                success = false;
                for(let i=0;i < bdField.length; i++){
                    if(bdField[i] === bodyField[j]){
                        success = true;
                        break;
                    }
                }
                if(!success)
                    break;
            }
            return success;
        }
    });

    productRouter.route('/product')
    .post((req, res) => {
        let product = new Product(req.body);
        product.save((err)=>{
            if(err){
                res.status(500).send(err);
            } else {
                res.json(product);
            }
        });
    });

    productRouter.use('/products',(req, res, next) =>{
        let queryParameters = {};
        let limit = 0;
        const parameters = Object.keys(req.query);
        console.log(parameters);
        if(parameters.length > 2){
            res.status(500).send('error in query parameters');
        }
        let error = false;
        for(let i = 0; i < parameters.length; i++){
            if( parameters[i] === 'company'){
                queryParameters = {...queryParameters,company: req.query.company};
            }else if(parameters[i] === 'line'){
                queryParameters = {...queryParameters, productLines: req.query.line};
            }else if(parameters[i] === 'category'){
                queryParameters = {...queryParameters, categories: req.query.category};
            }else if(parameters[i] === 'limit'){
                if (isNaN(req.query.limit)){
                    error= true;
                    break;
                } else {
                    limit = parseInt(req.query.limit);
                }
            } else {
                error = true;
                break;
            }
        }
        if(error){
            res.status(500).send('error in query parameters');
        } else {
            req.queryParameters = queryParameters;
            req.limit = limit;
            next();
        }
    });
    productRouter.route('/products')
    .get(async (req, res) => {
        console.log(req.queryParameters);
        if(req.limit<1){
            Product.find(req.queryParameters, (err, product) => {
                if(err) res.status(500).send(err); else res.json(product);
            });
        } else {
            Product.find(req.queryParameters).limit(req.limit).exec((err, product) => {
                if(err) res.status(500).send(err); else res.json(product);
            });
        }
    });
    
    module.exports = productRouter;
