const express = require('express'),
companyRouter = express.Router(),
db = require('mongoose').connect('mongodb://localhost/ProductInventory'),
Company = require('../Models/company.model');

//Routes CRUD in company

    companyRouter.route('/')
    .get((req, res, next) => {
        console.log('hello from api company');
        next();
    });
    companyRouter.route('/company')
    .post((req, res) => {
        let company = new Company(req.body);
        company.save((err) => {
            if (err) return console.error(err);
          });
        res.status(201).send(company);
    });
    companyRouter.use('/company/:id',(req, res, next) => {
        const id = req.params.id;
        Company.findById(id,(err, company) => {
            if(err){
                console.log(err);
                res.status(500).send(err);
            } else {
                req.company = company;
                next();
            }
        })
    });

    companyRouter.route('/company/:id')
    .get((req, res) => {
        res.json(req.company);
    })
    .put((req, res)=>{
        req.company.name = req.body.name;
        req.company.rif = req.body.rif;
        req.company.warehouse = req.body.warehouse;
        req.company.productLine = req.body.productLine;
        req.company.categories = req.body.categories;
        req.company.save((err)=>{
            if(err) {
                res.status(500).send(err)
            } else {
                res.json(req.company);
            }
        })
    })
    .patch((req, res)=>{
        if(req.body._id)
            delete req.body._id;
        for(let p in req.body){
            req.company[p] = req.body[p];
        }
        req.company.save((err) => {
            if(err)
                res.status(500).send(err);
            else{
                res.json(req.company);
            }
        })
    })
    .delete((req, res) =>{
        req.company.remove((err)=>{
            if(err){
                res.status(500).send(err);
            } else {
                res.status(204).send('Removed');
            }
        })
    });
    companyRouter.use('/companies',(req, res, next)=>{
        let queryParameters = {};
        let limit = 0;
        const parameters = Object.keys(req.query);
        console.log(parameters);
        let error = false;
        if(parameters[0] === 'limit'){
            if (isNaN(req.query.limit))
                error= true;
            else 
                limit = parseInt(req.query.limit);
        } else
            error = true;
        if(error)
            res.status(500).send('error in query parameters');
        else {
            req.queryParameters = queryParameters;
            req.limit = limit;
            next();
        }
    })
    companyRouter.route('/companies')
    .get((req, res) => {
        if(req.limit < 1){
            Company.find({},(err, companies)=>{
                if(err){
                    console.log(err);
                    res.status(500).send(err);
                } else {
                    res.json(companies);
                }
            });
        } else {
            Company.find({}).limit(req.limit).exec((err, companies)=>{
                if(err){
                    console.log(err);
                    res.status(500).send(err);
                } else {
                    res.json(companies);
                }
            });
        }
    });
    module.exports = companyRouter;
