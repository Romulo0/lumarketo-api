const mongoose = require('mongoose'),
Schema = mongoose.Schema,
ObjectId = mongoose.Schema.Types.ObjectId;

const SizeSchema = new Schema({
    whith: Number,
    height: Number
});

const TallaAndColorSchema = new Schema({
    Color: String,
    Talla: Number
});

const ImageSchema = new Schema({
    path: String,
    alt: String,
    size: SizeSchema
});

const EvaluationSchema = new Schema({
    userName: String,
    point: Number,
    comment: String
})

const ProductSchema = new Schema({
    company: ObjectId,
    serieNumber: String,
    name: String,
    detail: String,
    images: [ImageSchema],
    evaluations: [EvaluationSchema],
    size: SizeSchema,
    weight: Number,
    hasDiscount: Boolean,
    discount: Number,
    priceWithDiscount: Number,
    priceWithoutDiscount: Number,
    priceWithIVA: Number,
    priceWithoutIVA: Number,
    productLines: [String],
    categories: [String],
    hasColor: Boolean,
    tallaAndColor: TallaAndColorSchema,
    Quantity: Number,
    hasWarehouse: Boolean,
    warehouse: String
});

const Product = mongoose.model('Product',ProductSchema);

module.exports = Product;
