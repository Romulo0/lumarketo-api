const mongoose = require('mongoose'),
Schema = mongoose.Schema;

const WarehouseSchema = new Schema ({
    name: String,
    region: String,
    direction: String
});

/*const ProductLinesSchema = new Schema ({
    name: String
});

const CategoriesSchema = new Schema ({
    name: String
});*/

const RifSchema = new Schema ({
    type: String,
    rifId: String
});
const CompanySchema = new Schema({
    name: String,
    rif: {type:RifSchema, unique:true},
    warehouse: [WarehouseSchema],
    productLine: [String],
    categories: [String],
    description: String,
    contact:[String]
});

const Company = mongoose.model('Company', CompanySchema);

module.exports = Company;