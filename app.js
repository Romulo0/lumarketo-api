const express = require('express'),
app = express(),
path = require('path'),
morgan = require('morgan'),
mongoose = require('./db'),
productRouter = require('./Routes/product.routes'),
companyRoutes = require('./Routes/company.routes');
//Settings
app.set('port', process.env.PORT || 4001);

//Middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
//Routes
app.use('/api',productRouter);
app.use('/api',companyRoutes);

app.get('/',(req, res)=>{
    res.send('bienvenido');
}).listen(app.get('port'),()=>{
    console.log(`server running on port ${app.get('port')}`);
});